Rails.application.routes.draw do
  resources :students
  resources :school_years do
    member do
      get :export
      put :change_school_year
      delete :delete_students
    end
  end
  delete 'logout', to: 'sessions#destroy', as: :logout
  get 'login', to: 'sessions#new', as: :login
  resources :users, only: [:index, :new, :create, :edit, :update, :destroy] do
      member do
        patch :change_password
      end
    end
  resources :sessions, only: [:create]
  root to: 'main#home'
end
