class CreateSchoolYears < ActiveRecord::Migration[5.1]
  def change
    create_table :school_years do |t|
      t.string :name, null: false
      t.integer :level, null: false

      t.timestamps
    end

    add_index :school_years, [:name, :level], unique: true
  end
end
