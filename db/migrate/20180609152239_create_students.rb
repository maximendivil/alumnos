class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.integer :dni, null: false
      t.integer :gender, null: false
      t.belongs_to :school_year, index: true

      t.timestamps
    end

    add_index :students, :dni, unique: true
  end
end
