require 'test_helper'

class SchoolYearTest < ActiveSupport::TestCase
  def setup
    @first_of_primary = school_years(:first_of_primary)
    @first_of_secondary = school_years(:first_of_secondary)
  end

  test "name should be present" do
    @first_of_primary.name = nil
    assert_not @first_of_primary.valid?
    assert_includes @first_of_primary.errors[:name], I18n::t("errors.messages.blank")
  end

  test "name should be unique by level" do
    @first_of_primary.name = @first_of_secondary.name
    @first_of_primary.level = @first_of_secondary.level
    assert_not @first_of_primary.valid?
    assert_includes @first_of_primary.errors[:name], I18n::t("errors.messages.taken")
  end

  test "level should be present" do
    @first_of_primary.level = nil
    assert_not @first_of_primary.valid?
    assert_includes @first_of_primary.errors[:level], I18n::t("errors.messages.blank")
  end

  test "fixture should be valid" do
    assert @first_of_primary.valid?
  end
end
