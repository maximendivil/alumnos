require 'test_helper'

class StudentTest < ActiveSupport::TestCase
  def setup
    @student_one = students(:student_one)
    @student_two = students(:student_two)
  end

  test "first name should be present" do
    @student_one.first_name = nil
    assert_not @student_one.valid?
    assert_includes @student_one.errors[:first_name], I18n::t("errors.messages.blank")
  end

  test "last name should be present" do
    @student_one.last_name = nil
    assert_not @student_one.valid?
    assert_includes @student_one.errors[:last_name], I18n::t("errors.messages.blank")
  end

  test "dni should be unique" do
    @student_one.dni = @student_two.dni
    assert_not @student_one.valid?
    assert_includes @student_one.errors[:dni], I18n::t("errors.messages.taken")
  end

  test "gender should be present" do
    @student_one.gender = nil
    assert_not @student_one.valid?
    assert_includes @student_one.errors[:gender], I18n::t("errors.messages.blank")
  end

  test "school year should be present" do
    @student_one.school_year = nil
    assert_not @student_one.valid?
  end

  test "fixture should be valid" do
    @student_one.school_year = school_years(:first_of_primary)
    assert @student_one.valid?
  end
end
