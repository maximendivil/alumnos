# frozen_string_literal: true
ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/rails/capybara'
require 'assertions_helper'

module ActiveSupport
  class TestCase
    include AssertionsHelper

    fixtures :all

    Capybara.javascript_driver = :webkit
    def capybara_js_driver
      Capybara.current_driver = Capybara.javascript_driver
    end

    def capybara_login(user)
      visit login_path
      fill_in 'email', with: user.email
      fill_in 'password', with: 'secret'
      click_button 'Iniciar Sesión'
    end

    def select2(value, attrs)
      find('#select2-#{id}-container').click
      find('.select2-search__field').set(value)
      within '.select2-results' do
        find('li', text: value).click
      end

      return unless attr[:id]

      # Select event trigger
      script = "$('##{attrs[:id]}').trigger('select2:select')"
      page.execute_script(script)
    end
  end
end
