# frozen_string_literal: true
class UserPolicy < ApplicationPolicy

  def index?
    true
  end

  def new?
    create?
  end

  def edit?
    update?
  end

  def update?
    user == record
  end

  def create?
    true
  end

  def destroy?
    user != record
  end

  def change_password?
    user == record
  end
end
