class StudentFilter
  include ActiveModel::Model

  attr_accessor :name, :dni, :school_year_id, :deleted

  def call
    if @deleted.present? && @deleted == "1"
      students = Student.all
    else
      students = Student.not_deleted
    end

    students = students.where("LOWER(first_name) LIKE :name OR LOWER(last_name) LIKE :name", name: "%#{@name.downcase}%") if @name.present?
    students = students.where(dni: @dni) if @dni.present?
    students = students.where(school_year_id: @school_year_id) if @school_year_id.present?

    students
  end

end
