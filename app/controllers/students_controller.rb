class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  def index
    @presenter = StudentsPresenter.new(params)
  end

  def show
  end

  def new
    @student = Student.new
  end

  def edit
  end

  def create
    @student = Student.new(student_params)

    if @student.save
      redirect_to new_student_path, notice: 'El alumno se ha creado exitosamente'
    else
      flash[:error] = 'Ocurrió un error al crear el alumno. Por favor, intente nuevamente'
      render :new
    end
  end

  def update
    if @student.update(student_params)
      redirect_to students_path, notice: 'El alumno se ha modificado exitosamente.'
    else
      flash[:notice] = 'Ocurrió un error al modificar el alumno. Por favor, intente nuevamente'
      render :edit
    end
  end

  def destroy
    if @student.destroy
      flash[:notice] = 'El alumno se ha eliminado exitosamente'
    else
      flash[:notice] = 'Ocurrió un error al eliminar el alumno. Por favor, intente nuevamente'
    end
    redirect_to students_path
  end

  private

  def set_student
    @student = Student.find(params[:id])
  end

  def student_params
    params.require(:student).permit(:first_name, :last_name, :dni, :gender, :school_year_id)
  end
end
