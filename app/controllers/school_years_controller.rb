class SchoolYearsController < ApplicationController
  before_action :set_school_year, only: [:show, :edit, :update, :destroy, :export, :change_school_year, :delete_students]
  before_action :set_school_year_decorate, only: [:export]

  def index
    @school_years = SchoolYear.all.page(params[:page]).decorate
  end

  def show
    @presenter = SchoolYearPresenter.new(@school_year)
  end

  def new
    @school_year = SchoolYear.new
  end

  def edit
  end

  def create
    @school_year = SchoolYear.new(school_year_params)

    if @school_year.save
      redirect_to school_years_path, notice: 'El grado se ha creado exitosamente.'
    else
      flash[:error] = 'Ocurrió un error al crear el curso. Por favor, intente nuevamente'
      render :new
    end
  end

  def update
    if @school_year.update(school_year_params)
      redirect_to school_years_path, notice: 'El grado se ha modificado exitosamente.'
    else
      flash[:error] = 'Ocurrió un error al modificar el grado. Por favor, intente nuevamente'
      render :edit
    end
  end

  def destroy
    if @school_year.destroy
      flash[:notice] = 'El grado se ha eliminado exitosamente'
    else
      flash[:error] = 'Ocurrió un error al eliminar el grado. Por favor, intente nuevamente'
    end
    redirect_to school_years_path
  end

  def export
    render xlsx: 'export',
           filename: "#{@school_year.complete_name}.xlsx",
           locals: {xlsx_use_shared_strings: true}
  end

  def change_school_year
    student_ids = params[:school_year][:student_ids].reject(&:blank?)
    school_year_id = params[:school_year_id]
    if Student.where(id: student_ids).update(school_year_id: school_year_id)
      flash[:notice] = 'Los alumnos se cambiaron de grado exitosamente'
    else
      flash[:error] = 'Ocurrió un error al cambiar a los alumnos de grado. Por favor, intente nuevamente'
    end
    redirect_to @school_year
  end

  def delete_students
    student_ids = params[:school_year][:student_ids].reject(&:blank?)
    if Student.where(id: student_ids).update(deleted_at: Time.now)
      flash[:notice] = 'Los alumnos se sacaron del grado exitosamente'
    else
      flash[:error] = 'Ocurrió un error al sacar a los alumnos de grado. Por favor, intente nuevamente'
    end
    redirect_to @school_year
  end

  private

  def set_school_year
    @school_year = SchoolYear.find(params[:id])
  end

  def set_school_year_decorate
    @school_year = @school_year.decorate
  end

  def school_year_params
    params.require(:school_year).permit(:name, :level, :student_ids)
  end
end
