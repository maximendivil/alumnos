class SessionsController < ApplicationController
  skip_before_action :require_login, only: [:new, :create]
  layout 'sign_in'

  def new; end

  def create
    if @user = login(params[:email], params[:password])
      redirect_back_or_to(:root, notice: t(:login_successful))
    else
      flash.now[:alert] = t(:login_error)
      redirect_to login_path
    end
  end

  def destroy
    logout
    redirect_to login_path, notice: t(:logout_successful)
  end

end
