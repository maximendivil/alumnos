class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy change_password]

  def index
    authorize User
    @users = User.all.page(params[:page])
  end

  def new
    authorize User
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    authorize @user

    if @user.save
      redirect_to users_path, notice: '¡El usuario se creó exitosamente!'
    else
      flash[:error] = '¡Ups! Hubo un error al intentar crear el usuario.'
      render 'new'
    end
  end

  def edit
    authorize @user
  end

  def update
    authorize @user
    if @user.update(user_params)
      redirect_to root_path, notice: '¡El usuario se modificó exitosamente!'
    else
      flash[:error] = '¡Ups! Hubo un error al intentar modificar el usuario.'
      render :edit
    end
  end

  def change_password
    authorize @user
    if @user.valid_password? params[:user][:current_password]
      if @user.update(user_params)
        redirect_to root_path, notice: t(:success)
      else
        flash[:error] = '¡Ups! No se ha podido cambiar la clave.
                        Por favor intente nuevamente'
        render :edit
      end
    else
      @user.errors[:current_password] << 'La clave actual es incorrecta'
      render :edit
    end
  end

  def destroy
    authorize @user
    if @user.destroy
      redirect_to users_path, notice: t(:success)
    else
      flash[:error] = '¡Ups! No se ha podido eliminar el usuario.
                      Por favor intente nuevamente'
      redirect_to time_records_path
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
