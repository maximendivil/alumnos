require 'active_support/concern'
module SoftDestroyable
  extend ActiveSupport::Concern

  included do

    # -- Scopes
    scope :not_deleted, -> { where(deleted_at: nil) }
    scope :deleted, -> { where('deleted_at IS NOT NULL') }

    def permanent_destroy?
      false
    end

    def soft_delete
      self.update!(deleted_at: Time.zone.now)
    end

    def destroy
      if permanent_destroy?
        super
      else
        soft_delete
      end
    end

  end

end
