class SchoolYear < ApplicationRecord

  # -- Associations
  has_many :students

  # -- Scopes
  default_scope { order(:level, :name)}

  # -- Enums
  enum level: %i[initial primary secondary]

  # -- Validations
  validates :name, presence: true
  validates :name, uniqueness: { scope: [:level] }
  validates :level, presence: true
end
