class Student < ApplicationRecord
  include SoftDestroyable

  # -- Scopes
  default_scope { order(:gender, :last_name, :first_name)}
  scope :males, -> { where(gender: Student.genders[:male]) }
  scope :females, -> { where(gender: Student.genders[:female]) }

  # -- Enums
  enum gender: { male: 0, female: 1 }

  # -- Associations
  belongs_to :school_year

  # -- Validations
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :dni, presence: true, length: { minimum: 8, maximum: 8 }
  validates :dni, uniqueness: true
  validates :dni, numericality: { only_integer: true }
  validates :gender, presence: true
end
