class StudentDecorator < Draper::Decorator
  delegate_all
  decorates_association :school_year

  def complete_name
    "#{last_name} #{first_name}"
  end

  def first_name
    student.first_name.titleize
  end

  def last_name
    student.last_name.titleize
  end

  def gender_name
    I18n::t("student_genders.#{student.gender}")
  end

  def school_year_name
    return '-' if student.deleted_at.present?
    student.school_year.decorate.complete_name
  end

  private

  def student
    object
  end

end
