class SchoolYearDecorator < Draper::Decorator
  delegate_all
  decorates_association :students, scope: :not_deleted

  def complete_name
    "#{school_year.name} - #{level_name}"
  end

  def level_name
    I18n::t("school_year_levels.#{school_year.level}")
  end

  private

  def school_year
    object
  end

end
