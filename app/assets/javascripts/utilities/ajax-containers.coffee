App.initAjaxContainers = ->
  containers = $("[data-behavior~=ajax-container]")

  containers.each (index, el) ->
    url = $(el).data('url')

    # Si tiene un wrapper box le pongo un loading (AdminLte)
    box = $(el).closest('.box')
    box.append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>')

    $.get url, (data) =>
      $(el).html data
      box.find('.overlay').remove()

