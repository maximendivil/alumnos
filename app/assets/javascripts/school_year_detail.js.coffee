class SchoolYearDetail

  constructor: ->
    @bindEvents()
    @enableChangeSchoolYearForm()

  bindEvents: ->

    $("#js-select-all").on 'change', (e) =>
      if $("#js-select-all").prop('checked')
        $(".js-student-check").prop('checked', true).trigger('change')
      else
        $(".js-student-check").prop('checked', false).trigger('change')
      @enableChangeSchoolYearForm()

    $('.js-student-check').on 'change', (e) =>
      @setStudentsToChange()
      @enableChangeSchoolYearForm()
      if $('.js-student-check:checked').length == 0
        $("#js-select-all").prop('checked', false)

  setStudentsToChange: ->
    students = $('.js-student-check:checked')
    selectStudents = $('.js-student-ids')
    selectStudents.find('option').remove()
    selectStudents.append($('<option>', { value: $(student).data('studentId'), selected: true })) for student in students

  enableChangeSchoolYearForm: ->
    if $('.js-student-check:checked').length > 0
      $('.js-student-action').show()
      $('#student_school_year').next(".select2-container").show()
    else
      $('.js-student-action').hide()
      $('#student_school_year').next(".select2-container").hide()


$(document).on "turbolinks:load", ->
  App.school_year_detail = new SchoolYearDetail() unless $(".school_years.show").length == 0
