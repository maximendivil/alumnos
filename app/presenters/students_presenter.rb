class StudentsPresenter

  def initialize(params)
    @params = params
  end

  def students
    @students ||= filter.call.page(@params[:page]).decorate
  end

  def filter
    @filter ||= StudentFilter.new(filter_params)
  end

  private

  def filter_params
    if @params[:student_filter]
      @params.require(:student_filter).permit(:name, :dni, :school_year_id, :deleted)
    else
      {}
    end
  end
end
