class SchoolYearPresenter

  def initialize(school_year)
    @school_year = school_year
  end

  def male_students
    @male_students ||= @school_year.students.not_deleted.males
  end

  def female_students
    @female_students ||= @school_year.students.not_deleted.females
  end

  def students
    @students ||= @school_year.students.not_deleted.decorate
  end

  def school_year
    @school_year
  end

  def school_year_decorated
    @school_year_decorate ||= @school_year.decorate
  end

  def has_students?
    @school_year.students.not_deleted.any?
  end
end
